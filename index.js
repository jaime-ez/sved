'use strict'

const identity = require('./lib/identity')
const encrypt = require('./lib/encrypt')
const decrypt = require('./lib/decrypt')

const https = require('https')
const selfsigned = require('selfsigned')
const Koa = require('koa')
const _ = require('koa-route')
const bodyParser = require('koa-bodyparser')
const app = new Koa()
app.use(bodyParser())

const index = {
  encrypt: (ctx) => {
    return encrypt(ctx.request.body)
    .then((value) => {
      ctx.body = value
    })
    .catch((err) => {
      if (err.message === '400') {
        ctx.throw(400)
      } else {
        ctx.throw(err)
      }
    })
  },
  decrypt: (ctx) => {
    return decrypt(ctx.request.body)
    .then((value) => {
      ctx.body = value
    })
    .catch((err) => {
      if (err.message === '400') {
        ctx.throw(400)
      } else {
        ctx.throw(err)
      }
    })
  }
}

app.use(_.post('/encrypt', index.encrypt))
app.use(_.post('/decrypt', index.decrypt))

identity()
.then(() => {
  // create ssl certs and server
  let pems = selfsigned.generate([{ name: 'commonName', value: 'svde' }], { days: 10000 })
  https.createServer({
    key: pems.private,
    cert: pems.cert
  }, app.callback()).listen(8000)
  console.log('listening over https on port 8000')
})
.catch((err) => {
  console.error('identityError:', err)
})
