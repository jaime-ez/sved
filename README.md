# Encrypt Decrypt service  

This module exposes an API that performs encryption and decryption of messages based on conditions.

The module is to be used within the Celula network instances in order to preserve confidentiality of the secret keys employed for the cryptographic operations.  

The use case for this module is encryption as a service: a user can ask for encryption of a sensitive data, which can then be transferred securely to a third party who can decrypt it only if a condition is satisfied.  

The current prototype implementation supports only timestamps as conditions, but other use cases can be implemented such as decrypt based on the transfer of cryptocurrencies to a given account, publication of a certain file in the Dat network... anything that can be verified and accessed through an API.  

## Setup  

### As part of the celula network  

Make a `POST` replication request to a valid Celula instance with the mandatory request body and indicate the Encrypt Decrypt service repositoryUrl on the corresponding field.  

### Local  

For testing purposes:  

`npm install && npm start`  

## API  

### `POST` /encrypt  

Encrypts the given message in the body request and sets the decryption condition.  

#### The body of the request must contain the following:  

```
{
  message: {
  }
  condition: {
    timestamp: 'ISO DATE'
  }
}
```

The message field can be any object, for example: {text: 'hi'} and the timestamp must be an ISO formatted date, after which the encrypted message can be decrypted.  

#### Example Response:  

```
{
  id: 'f020817f-6c77-4f27-be00-2ec1929c472e',
  encrypted: 'YmIncPacpOjboZv4cJYb10saQufZCFMHSnBoJg==',
  condition: { timestamp: '2017-12-02T00:33:50.455Z' },
  signature: 'J/kQxZSnT75lvJbmd9Y9+HnP1XO6G4Q+kgT48lZanZMXHVtkP62/SqY/zbzHpBQ7R/Baj0eBZaMMt2+XUGPrDA=='
}
```
