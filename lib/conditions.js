'use strict'

let selector = (condition) => {
  if (condition.timestamp) {
    return timestamp(condition.timestamp)
  }
  return Promise.reject(new Error('400'))
}

let timestamp = (stamp) => {
  return new Promise((resolve, reject) => {
    let expected = new Date(stamp).getTime()
    let current = new Date().getTime()
    if (current > expected) {
      resolve()
    } else {
      reject(new Error('401'))
    }
  })
}

module.exports = selector
