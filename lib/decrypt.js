'use strict'

const encryption = require('sodium-encryption')
const level = require('level')
const verify = require('./verify')
const conditions = require('./conditions')

let index = (opts) => {
  return new Promise((resolve, reject) => {
    if (!(opts.id && typeof opts.id === 'string' && opts.encrypted && typeof opts.encrypted === 'string' && opts.condition && typeof opts.condition === 'object' && opts.signature && typeof opts.signature === 'string')) {
      return reject(new Error('400'))
    }

    // verify signature
    let verifyRequest = {
      message: {
        id: opts.id,
        encrypted: opts.encrypted,
        condition: opts.condition
      },
      signature: opts.signature
    }

    verify(verifyRequest)
    .then((verified) => {
      if (verified) {
        conditions(opts.condition)
        .then(() => {
          decrypt(opts)
          .then((value) => {
            resolve(value)
          })
          .catch((err) => {
            reject(err)
          })
        })
        .catch((err) => {
          reject(err)
        })
      } else {
        reject(new Error('401'))
      }
    })
    .catch((err) => {
      reject(err)
    })
  })
}

let decrypt = (opts) => {
  return new Promise((resolve, reject) => {
    let db = level('./db', {valueEncoding: 'json'})
    db.get(opts.id, (err, value) => {
      if (err) {
        db.close(() => {
          reject(err)
        })
      } else {
        db.close((err) => {
          if (err) {
            console.error(err)
          }
          let decrypted = encryption.decrypt(Buffer.from(opts.encrypted, 'base64'), Buffer.from(value.nonce.data), Buffer.from(value.key.data))
          if (!decrypted) {
            return reject(new Error('400'))
          }
          resolve(JSON.parse(decrypted.toString('utf-8')))
        })
      }
    })
  })
}

module.exports = index
