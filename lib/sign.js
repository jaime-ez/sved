'use strict'

const signatures = require('sodium-signatures')
const level = require('level')

let sign = (opts) => {
  return new Promise((resolve, reject) => {
    if (!opts || typeof opts !== 'object') {
      return reject(new Error('400'))
    }
    let db = level('./db', {valueEncoding: 'json'})
    db.get('keyPair', (err, value) => {
      if (err) {
        db.close(() => {
          reject(err)
        })
      } else {
        db.close((err) => {
          if (err) {
            console.error(err)
          }
          let signature = signatures.sign(Buffer.from(JSON.stringify(opts)), Buffer.from(value.secretKey.data))
          resolve(signature.toString('base64'))
        })
      }
    })
  })
}

module.exports = sign
