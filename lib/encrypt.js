'use strict'

const level = require('level')
const encryption = require('sodium-encryption')
const uuid = require('uuid')
const sign = require('./sign')

let encrypt = (opts) => {
  return new Promise((resolve, reject) => {
    if (!(opts.message && typeof opts.message === 'object' && opts.condition && opts.condition.timestamp && typeof opts.condition.timestamp === 'string' && Object.prototype.toString.call(new Date(opts.condition.timestamp)) === '[object Date]')) {
      return reject(new Error('400'))
    }
    let db = level('./db', {valueEncoding: 'json'})
    let dbError = null
    let key = encryption.key()
    let nonce = encryption.nonce()
    let encryptedMessage = encryption.encrypt(Buffer.from(JSON.stringify(opts.message)), nonce, key)
    let id = uuid.v4()

    db.put(id, {key, nonce}, (err) => {
      if (err) {
        console.error(err)
        dbError = err
      } else {
        db.close((err) => {
          if (err) {
            console.error(err)
          }
          if (dbError) {
            return reject(dbError)
          }
          let signOpts = {id, encrypted: encryptedMessage.toString('base64'), condition: opts.condition}

          sign(signOpts)
          .then((value) => {
            resolve({id, encrypted: encryptedMessage.toString('base64'), condition: opts.condition, signature: value})
          })
          .catch((err) => {
            reject(err)
          })
        })
      }
    })
  })
}

module.exports = encrypt
