'use strict'

const signatures = require('sodium-signatures')
const level = require('level')

let verify = (opts) => {
  return new Promise((resolve, reject) => {
    if (!(opts.message && typeof opts.message === 'object' && opts.signature && typeof opts.signature === 'string')) {
      return reject(new Error('400'))
    }

    let db = level('./db', {valueEncoding: 'json'})
    db.get('keyPair', (err, value) => {
      if (err) {
        db.close(() => {
          reject(err)
        })
      } else {
        db.close((err) => {
          if (err) {
            console.error(err)
          }
          let verified = signatures.verify(Buffer.from(JSON.stringify(opts.message)), Buffer.from(opts.signature, 'base64'), Buffer.from(value.publicKey.data))

          resolve(verified)
        })
      }
    })
  })
}

module.exports = verify
