'use strict'

const signatures = require('sodium-signatures')
const level = require('level')

let identity = () => {
  return new Promise((resolve, reject) => {
    const db = level('./db', {valueEncoding: 'json'})
    let dbError = null
    db.get('keyPair', (err, value) => {
      if (err) {
        if (err.notFound) {
            // create key pair
          let keyPair = signatures.keyPair()
          db.put('keyPair', keyPair, (err) => {
            if (err) {
              console.error(err)
              dbError = err
            } else {
              db.close((err) => {
                if (err) {
                  console.error(err)
                }
                if (dbError) {
                  return reject(dbError)
                }
                resolve()
              })
            }
          })
        } else {
          db.close(() => {
            reject(err)
          })
        }
      } else {
        db.close((err) => {
          if (err) {
            console.error(err)
          }
          resolve()
        })
      }
    })
  })
}

module.exports = identity
