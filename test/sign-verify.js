'use strict'

const test = require('tape')
const sign = require('../lib/sign')
const verify = require('../lib/verify')

test('sign-verify', (t) => {
  let m = {
    hi: 'you'
  }
  sign(m)
  .then((value) => {
    if (typeof value === 'string') {
      verify({
        message: m,
        signature: value
      })
      .then((value) => {
        if (value) {
          t.pass('ok')
        } else {
          t.fail()
        }
        t.end()
      })
      .catch((err) => {
        t.fail(err)
        t.end()
      })
    } else {
      t.fail()
      t.end()
    }
  })
  .catch((err) => {
    t.fail(err)
    t.end()
  })
})
