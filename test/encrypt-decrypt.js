'use strict'

const test = require('tape')
const encrypt = require('../lib/encrypt')
const decrypt = require('../lib/decrypt')

test('encrypt-decrypt success', (t) => {
  let message = {
    hi: 'you'
  }
  let ctx = {message, condition: {timestamp: new Date().toISOString()}}
  encrypt(ctx)
  .then((value) => {
    if (typeof value.id === 'string' && typeof value.encrypted === 'string') {
      decrypt(value)
      .then((value) => {
        if (value.hi === 'you') {
          t.pass('ok')
        } else {
          t.fail()
        }
        t.end()
      })
      .catch((err) => {
        t.fail(err)
        t.end()
      })
    } else {
      t.fail()
      t.end()
    }
  })
  .catch((err) => {
    t.fail(err)
    t.end()
  })
})

test('encrypt-decrypt fail on timestamp', (t) => {
  let message = {
    hi: 'you'
  }
  let time = new Date().getTime() + 1000
  let ctx = {message, condition: {timestamp: new Date(time).toISOString()}}
  encrypt(ctx)
  .then((value) => {
    if (typeof value.id === 'string' && typeof value.encrypted === 'string') {
      decrypt(value)
      .then((value) => {
        t.fail()
        t.end()
      })
      .catch(() => {
        t.pass('ok')
        t.end()
      })
    } else {
      t.fail()
      t.end()
    }
  })
  .catch((err) => {
    t.pass(err)
    t.end()
  })
})
